# Assignment 2 : Functions in 2D

## [Course material](assignment2.ipynb)

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Content is licensed under the <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons License Attribution - NonCommercial 4.0 International</a>.

